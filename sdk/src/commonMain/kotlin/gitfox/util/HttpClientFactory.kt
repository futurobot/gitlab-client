package gitfox.util

import gitfox.entity.app.session.AuthHolder
import io.github.aakira.napier.Napier
import io.ktor.client.*
import io.ktor.client.engine.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.*
import io.ktor.http.auth.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json

internal class HttpClientFactory(
    private val engineFactory: (cacheSize: Long, timeout: Long) -> HttpClientEngine
) {
    fun create(
        json: Json,
        authData: AuthHolder?,
        enableLogging: Boolean
    ): HttpClient = HttpClient(engineFactory(CACHE_SIZE_BYTES, TIMEOUT)) {
        if (enableLogging) {
            install(Logging) {
                level = LogLevel.ALL
                logger = object : Logger {
                    override fun log(message: String) {
                        Napier.v(tag = "HttpClient", message = message)
                    }
                }
            }
        }
        install(ContentNegotiation) {
            json(json)
        }
        install(Auth) {
            providers.add(object : AuthProvider {
                override val sendWithoutRequest = true
                override fun isApplicable(auth: HttpAuthHeader) = true

                override suspend fun addRequestHeaders(request: HttpRequestBuilder) {
                    authData?.let {
                        if (authData.isOAuth) {
                            request.headers["Authorization"] = "Bearer ${authData.token}"
                        } else {
                            request.headers["PRIVATE-TOKEN"] = authData.token.toString()
                        }
                    }
                }

            })
        }
    }

    companion object {
        private const val CACHE_SIZE_BYTES = 20 * 1024L
        private const val TIMEOUT = 30L
    }
}
